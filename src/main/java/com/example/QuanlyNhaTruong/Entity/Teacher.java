package com.example.QuanlyNhaTruong.Entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
public class Teacher extends Person {
    private int namKinhNghiem;
    private boolean honNhan;

    public Teacher() {
    }

    public Teacher(int tuoi, String ten, String gioiTinh, int namKinhNghiem, boolean honNhan) {
        super(tuoi, ten, gioiTinh);
        this.namKinhNghiem = namKinhNghiem;
        this.honNhan = honNhan;
    }

    public Teacher(int tuoi, String ten, String gioiTinh, boolean honNhan) {
        super(tuoi, ten, gioiTinh);
        this.honNhan = honNhan;
    }

    public int getNamKinhNghiem() {
        return namKinhNghiem;
    }

    public void setNamKinhNghiem(int namKinhNghiem) {
        this.namKinhNghiem = namKinhNghiem;
    }

    public boolean isHonNhan() {
        return honNhan;
    }

    public void setHonNhan(boolean honNhan) {
        this.honNhan = honNhan;
    }
}
