package com.example.QuanlyNhaTruong.Controller;

import com.example.QuanlyNhaTruong.Service.StudentInter;
import com.example.QuanlyNhaTruong.Service.TeacherInter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/calling")
public class Controller {

//    Url:
//    http://localhost:8080/calling/sayHelloStudent
//    http://localhost:8080/calling/introduceStudent?age=16&name=Minh

    @Autowired
    private StudentInter student;

    @Autowired
    private TeacherInter teacher;

    @GetMapping("/studentNoParam")
    public ResponseEntity<?> createStudentNoParam() {
        return new ResponseEntity<>(student.createStudentNoParams(), HttpStatus.OK);
    }

    @GetMapping("/createStudentWithLopAndTen")
    public ResponseEntity<?> createStudentWithLopAndTen() {
        return new ResponseEntity<>(student.createStudentWithLopAndTen(), HttpStatus.OK);
    }

    @GetMapping("/createStudentWithAllAttributes")
    public ResponseEntity<?> createStudentWithAllAttributes() {
        return new ResponseEntity<>(student.createStudentWithAllAttributes(), HttpStatus.OK);
    }

    @GetMapping("/sayHelloStudent")
    public ResponseEntity<?> sayHelloStudent(){
        return new ResponseEntity<>(student.sayHello(), HttpStatus.OK);
    }

    @GetMapping("/introduceStudent")
    public ResponseEntity<?> introduceStudent(@RequestParam int age, @RequestParam String name){
        return new ResponseEntity<>(student.sayHello(age, name), HttpStatus.OK);
    }

    @GetMapping("/ageStudent")
    public ResponseEntity<?> ageStudent(@RequestParam int age){
        return new ResponseEntity<>(student.sayHello(age), HttpStatus.OK);
    }

    @GetMapping("/createTeacherWithAllAttributes")
    public ResponseEntity<?> createTeacherWithAllAttributes(){
        return new ResponseEntity<>(teacher.createTeacherWithAllAttributes(), HttpStatus.OK);
    }

    @GetMapping("/createTeacherWithMinimunAttributes")
    public ResponseEntity<?> createTeacherWithMinimunAttributes(){
        return new ResponseEntity<>(teacher.createTeacherWithMinimunAttributes(), HttpStatus.OK);
    }

    @GetMapping("/teacherSayHello")
    public ResponseEntity<?> teacherSayHello(){
        return new ResponseEntity<>(teacher.sayHello(), HttpStatus.OK);
    }

    @GetMapping("/introduceTeacher")
    public ResponseEntity<?> introduceTeacher(){
        return new ResponseEntity<>(teacher.sayHello(26,"Linh"), HttpStatus.OK);
    }

    @GetMapping("/ageTeacher")
    public ResponseEntity<?> ageTeacher(){
        return new ResponseEntity<>(teacher.sayHello(26), HttpStatus.OK);
    }
}
