package com.example.QuanlyNhaTruong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuanlyNhaTruongApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuanlyNhaTruongApplication.class, args);
	}

}
