package com.example.QuanlyNhaTruong.Service;

import com.example.QuanlyNhaTruong.Entity.Student;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface StudentInter {
    public Student createStudentNoParams();

    public Map<String, String> createStudentWithLopAndTen();

    public Map<String, Object> createStudentWithAgeNameAndGender();

    public Map<String, Object> createStudentWithAllAttributes();

    public String sayHello();

    public String sayHello(int tuoi, String ten);

    public String sayHello(int tuoi);

//    @Override
//    public void hello() {
//        // Implement the behavior for the hello() method in the Student class
//        System.out.println("Hello, I am a student.");
//    }
//    Khong the override duoc method Hello vi no co access private

}
