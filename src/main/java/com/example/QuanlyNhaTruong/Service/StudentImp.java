package com.example.QuanlyNhaTruong.Service;

import com.example.QuanlyNhaTruong.Entity.Student;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class StudentImp implements StudentInter {

    @Override
    public Student createStudentNoParams() {
        Student student = new Student();
        System.out.println(student);
        return student;
    }
    @Override
    public Map<String, String> createStudentWithLopAndTen() {
        Student student = new Student("11C","Minh");
        System.out.println(student.getLop());
        Map<String, String> studentMap = new HashMap<>();
        studentMap.put("lop", student.getLop());
        studentMap.put("ten", student.getTen());
        return studentMap;
    }

    @Override
    public Map<String, Object> createStudentWithAgeNameAndGender() {
        Student student = new Student(16,"Minh","Nam");
        Map<String, Object> studentMap = new HashMap<>();
        studentMap.put("tuoi", student.getTuoi());
        studentMap.put("ten", student.getTen());
        studentMap.put("diem", student.getDiem());
        return studentMap;
    }

    @Override
    public Map<String, Object> createStudentWithAllAttributes(){
        Student student = new Student("11C","Minh", 7);
        Map<String, Object> studentMap = new HashMap<>();
        studentMap.put("lop", student.getLop());
        studentMap.put("ten", student.getTen());
        studentMap.put("diem", student.getDiem());
        return studentMap;
    }

    @Override
    public String sayHello() {
        System.out.println("My info is secret");
        return "My info is secret";
    }
    @Override
    public String sayHello(int tuoi, String ten) {
        System.out.println("Hello, I am a student, my name "+ ten +", i am " + tuoi +" years old");
        return "Hello, I am a student, my name is "+ ten +", i am " + tuoi +" years old";
    }
    @Override
    public String sayHello(int tuoi) {
        System.out.println("Hello, I am a student,  i am "+ tuoi +" years old");
        return "Hello, I am a student,  i am "+ tuoi +" years old";
    }

//    @Override
//    public void hello() {
//        // Implement the behavior for the hello() method in the Student class
//        System.out.println("Hello, I am a student.");
//    }
//    Khong the override duoc method Hello vi no co access private
}
