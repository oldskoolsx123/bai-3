package com.example.QuanlyNhaTruong.Service;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface TeacherInter {

    public Map<String, Object> createTeacherWithAllAttributes() ;

    public Map<String, Object> createTeacherWithMinimunAttributes();
    public String sayHello() ;

    public String sayHello(int tuoi, String ten) ;

    public String sayHello(int tuoi) ;
}
