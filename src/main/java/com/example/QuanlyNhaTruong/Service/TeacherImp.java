package com.example.QuanlyNhaTruong.Service;

import com.example.QuanlyNhaTruong.Entity.Teacher;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
@Component
public class TeacherImp implements TeacherInter{

    public Map<String, Object> createTeacherWithAllAttributes() {
        Teacher teacher = new Teacher(26, "Hoa", "Nu", 3, false);
        Map<String, Object> teacherMap = new HashMap<>();
        teacherMap.put("lop", teacher.getTuoi());
        teacherMap.put("ten", teacher.getTen());
        teacherMap.put("gioi tinh", teacher.getGioiTinh());
        teacherMap.put("nam kinh nghiem", teacher.getNamKinhNghiem());
        teacherMap.put("honNhan", teacher.isHonNhan());
        return teacherMap;
    }

    public Map<String, Object> createTeacherWithMinimunAttributes() {
        Teacher teacher = new Teacher(32, "Dung", "Nu", false);
        System.out.println(teacher);
        Map<String, Object> teacherMap = new HashMap<>();
        teacherMap.put("lop", teacher.getTuoi());
        teacherMap.put("ten", teacher.getTen());
        teacherMap.put("gioi tinh", teacher.getGioiTinh());
        teacherMap.put("honNhan", teacher.isHonNhan());
        return teacherMap;
    }
    public String sayHello() {
        System.out.println("My info is secret");
        return "My info is secret";
    }

    public String sayHello(int tuoi, String ten) {
        System.out.println("Hello, I am a Teacher, my name "+ ten +", i am " + tuoi +" years old");
        return "Hello, I am a Teacher, my name "+ ten +", i am " + tuoi +" years old";
    }

    public String sayHello(int tuoi) {
        System.out.println("Hello, I am a Teacher,  i am "+ tuoi +" years old");
        return "Hello, I am a Teacher,  i am "+ tuoi +" years old";
    }
}
